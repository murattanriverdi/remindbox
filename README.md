# Remindbox


## Uygulamayı çalıştırmak için;

Uygulama dart dilinin frameworkü olan Flutter ile kodlandı.

Flutter uygulamasını çalıştırabilmek için ilk olarak Flutter SDK dosyasını [kendi sitesinden](https://flutter.dev/docs/get-started/install/windows) indirebilirsiniz. Farklı bir sürümü kullanmak istiyorsanız [arşivden](https://flutter.dev/docs/development/tools/sdk/releases) farklı bir sürümü kullanabilirsiniz. (Uygulama kodlanırken 2.0.1 SDK sürümü kullanıldı) Daha stabil bir sonuç almak adına uygulamanın kodlandığı  SDK sürümünü indirmeniz önerilir, aksi takdirde uygulama içerisinde bulunan pubspec.yaml dosyası içerisinde bulunan ,
*__environment:__*
    *__sdk: ">=2.7.0 <3.0.0"__* 

şu satırda belirtilen sürüm ile uyumsuzluk yaşanabilir.


Daha sonra indirilen zip uzantılı dosyayı zipten çıkarıp bilgisayar ortamında bir yolda saklayınız (Örn: C:\flutter gibi).

Daha sonra flutter'ın bu SDK'yı kullanabilmesi için bunu Ortam Değişkenleri(Path Variables) kısmına ekleyip tanıtmamız gerekiyor.


Bunun için sırasıyle aşağıdaki adımları izleyip flutter SDK'sını sistemimize ekliyoruz.


![](readme/images/1.png?raw=true)

![](readme/images/2.png?raw=true)

![](readme/images/3.png?raw=true)

![](readme/images/4.png?raw=true)

>Burada siz dosyayı nereye eklediyseniz oranın yolunu seçiniz, doğru yolu seçtiğinizden emin olunuz! Yeniden başlatma gerekebilir.


Daha sonra kurulum esnasında ihtiyacımız olan herhangi bir platform bağımlılıklarını görmek için herhangi bir console üzerinden *__flutter doctor__* komutunu çalıştırın.


Aşağıdaki gibi bir çıktı alacaksınız muhtemelen!




![](readme/images/5.png?raw=true)



Bağımlılık olarak Android Studio kurulumunu yapmamız gerekmektedir.



**ANDROID STUDIO KURULUMU**

[https://developer.android.com/studio](https://developer.android.com/studio) adresinden indirip kurulumu başlatabilirsiniz.


Kurulumu tamamladıktan sonra Android Studio giriş ekranından Flutter Pluginin kurabilirsiniz.


![](readme/images/6.png?raw=true)



![](readme/images/7.png?raw=true)



>Daha detaylı bilgi sahibi olmak için [Flutter kurulum  sayfasını](https://flutter.dev/docs/get-started/install/windows) ziyaret edebilirsiniz. 


Uygulamayı bir sanal emülator üzerinde derlemek için Android Studio üzerinden AVD Manager aracılığıyla sanal bir android cihaz ekleyebilirsiniz. Bu işlemi yapabilmeniz için cihazınızda sanallaştırmanın açık olması ve kurulu olması gerekmektedir.Herhangi bir problemde kablo aracılığıyla fiziksel bir cihazı bağlayıp onun üzerinden derleme işlemini yapabilirsiniz.



![](readme/images/8.png?raw=true)



![](readme/images/9.png?raw=true)


>Tekrardan  *__flutter doctor__* komutunu çalıştırdıktan sonra aşağıdaki ekrana
benzer bir ekran ile karşılaşmalısınız.


![](readme/images/10.png?raw=true)

>Resimde farklı IDE'lerin kurulumları gözükmektedir, kullandığınız  herhangi bir IDE'nin kurulumunu yapmanız yeterli olacaktır.



Gerekli kurulumları yaptıktan sonra artık uygulamamızı derleyebiliriz.



Uygulamamızın kaynak kodlarını açtıktan sonra dosyaların hata vermemesi için gerekli paketleri,bağımlılıkları indirmemiz gerekiyor.
Bunun için Run etmeden önce  *__flutter pub get__* komutunu çalıştırıp daha sonra Run ediniz.

 Run ettikten sonra uygulamanın çıktısını göreceksiniz.









