import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:remindbox/models/reminders.dart';
import 'package:remindbox/worker/dateOperation.dart';
import 'dart:io' show Platform;
import 'package:rxdart/subjects.dart';

class NotificationManager {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  var initSettings;
  BehaviorSubject<ReceiveNotification> get didReceiveNotificationSubject =>
      BehaviorSubject<ReceiveNotification>();

  NotificationManager.init() {
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

    if (Platform.isIOS) {
      requestIOSPermission();
    }
    initalizePlatform();
  }

  requestIOSPermission() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        .requestPermissions(alert: true, badge: true, sound: true);
  }

  initalizePlatform() {
    var initSettingAndroid = AndroidInitializationSettings('ic_launcher');
    var initSettingIOS = IOSInitializationSettings(
        requestSoundPermission: true,
        requestBadgePermission: true,
        requestAlertPermission: true,
        onDidReceiveLocalNotification: (id, title, body, payload) async {
          ReceiveNotification notification = ReceiveNotification(
              id: id, title: title, body: body, payload: payload);

          didReceiveNotificationSubject.add(notification);
        });

    initSettings = InitializationSettings(
        android: initSettingAndroid, iOS: initSettingIOS);
  }

  setOnNotificationReceive(Function onNotificationReceive) {
    didReceiveNotificationSubject.listen((notification) {
      onNotificationReceive(notification);
    });
  }

  setOnNotificationClick(Function onNotificationClick) async {
    await flutterLocalNotificationsPlugin.initialize(initSettings,
        onSelectNotification: (String payload) async {
      onNotificationClick(payload);
    });
  }

  Future<void> createNotification(Reminders reminder) async {
    var androidChannel = AndroidNotificationDetails(
        'CHANNEL_ID', 'CHANNEL_NAME', 'CHANNEL_DESCRIPTION',
        importance: Importance.max,
        priority: Priority.high,
        playSound: true,
        enableVibration: true,
        timeoutAfter: 500000,
        enableLights: true);

    var iosChannel = IOSNotificationDetails();
    var platformChanel =
        NotificationDetails(android: androidChannel, iOS: iosChannel);

    DateTime date = DateOperation()
        .dateConvertOperation(reminder.date + " " + reminder.time);
    date = date.add(Duration(seconds: 1, microseconds: 00001));
    Time time = new Time(date.hour, date.minute);
    var repeatType = reminder.repeatType;
    if (repeatType == 0) {
      await flutterLocalNotificationsPlugin.schedule(reminder.id,
          reminder.title, reminder.description, date, platformChanel,
          payload: "New Payload");
    } else if (repeatType == 1) {
      await flutterLocalNotificationsPlugin.showDailyAtTime(reminder.id,
          reminder.title, reminder.description, time, platformChanel,
          payload: "New Payload");
    } else if (repeatType == 2) {
      Day day = Day(date.weekday);
      await flutterLocalNotificationsPlugin.showWeeklyAtDayAndTime(reminder.id,
          reminder.title, reminder.description, day, time, platformChanel,
          payload: "New Payload");
    }
  }
}

NotificationManager notificationManager = NotificationManager.init();

class ReceiveNotification {
  final int id;
  final String title;
  final String body;
  final String payload;
  ReceiveNotification(
      {@required this.id,
      @required this.title,
      @required this.body,
      @required this.payload});
}
