import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:remindbox/controller/galleryController.dart';
import 'package:remindbox/models/gallery.dart';

class GalleryAddScreen extends StatefulWidget {
  @override
  _GalleryAddScreen createState() => _GalleryAddScreen();
}

class _GalleryAddScreen extends State<GalleryAddScreen> {
  File imageFile;

  @override
  void initState() {}
  _openGallery(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      imageFile = picture as File;
    });
    Navigator.of(context).pop();
  }

  _openCamera(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      imageFile = picture as File;
    });
    Navigator.of(context).pop();
  }

  Future<void> _showChoiseDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Birini Seçin"),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  GestureDetector(
                    child: Text("Galeri"),
                    onTap: () {
                      _openGallery(context);
                    },
                  ),
                  SizedBox(height: 20.0),
                  GestureDetector(
                    child: Text("Kamera"),
                    onTap: () {
                      _openCamera(context);
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget _decideImageView() {
    if (imageFile == null) {
      return Text("Resim seçilmedi");
    } else {
      return Image.file(imageFile);
    }
  }

  var _titleController = TextEditingController();
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  DateTime _dateTime = DateTime.now();

  _showErrorSnackBar(message) {
    var _snackBar = SnackBar(content: message, backgroundColor: Colors.red);
    _globalKey.currentState.showSnackBar(_snackBar);
  }

  _getTitleInput() {
    return TextField(
      controller: _titleController,
      decoration: InputDecoration(labelText: "Başlık", hintText: "Başlık"),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      appBar: AppBar(
        title: Text("Galeriye Resim Ekle"),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: [
                  _getTitleInput(),
                  SizedBox(height: 20.0),
                  _decideImageView(),
                  SizedBox(height: 20.0),
                  RaisedButton(
                    child: Text(
                      "Resim Seçin",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.pinkAccent,
                    onPressed: () {
                      _showChoiseDialog(context);
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  RaisedButton(
                    onPressed: () async {
                      //resmi kaydetme
                      var directory = await getApplicationDocumentsDirectory();
                      print("directory");
                      print(directory);
                      var path = directory.path;
                      var fileExtension = imageFile.toString().split(".").last;
                      var time = DateTime.now().millisecondsSinceEpoch;
                      var fileName = time.toString() + "." + fileExtension;
                      var copyPath = path + "/" + fileName;

                      final File newImage = await imageFile.copy('$copyPath');

                      var galleryObject = Gallery();
                      galleryObject.title = _titleController.text;
                      galleryObject.image = fileName;

                      var _galleryController = GalleryController();
                      var result =
                          await _galleryController.saveGallery(galleryObject);
                      if (result > 0) {
                        Navigator.pop(context, "success");
                      } else {
                        _showErrorSnackBar(
                            Text("Hata Oluştu Bilgilerinizi Kontrol Ediniz"));
                      }
                    },
                    color: Colors.blue,
                    child: Text(
                      "Kaydet",
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
