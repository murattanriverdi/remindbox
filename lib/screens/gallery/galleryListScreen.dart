import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:remindbox/controller/galleryController.dart';
import 'package:remindbox/models/gallery.dart';
import 'package:remindbox/screens/drawerScreen.dart';

class GalleryListScreen extends StatefulWidget {
  @override
  _GalleryListScreenState createState() => _GalleryListScreenState();
}

class _GalleryListScreenState extends State<GalleryListScreen> {
  List<Gallery> _galleryList = List<Gallery>();
  GalleryController _galleryController = GalleryController();
  var _imagePath;
  @override
  void initState() {
    _getGallery();
    _getDirectory();
  }

  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  _showSuccessSnackBar(message) {
    var _snackBar = SnackBar(
      content: message,
      backgroundColor: Colors.green,
    );
    _globalKey.currentState.showSnackBar(_snackBar);
  }

  _getGallery() async {
    _galleryList.clear();
    var gallerys = await _galleryController.getGallery();
    setState(() {
      gallerys.forEach((gallery) {
        var model = Gallery();
        model.id = gallery["id"];
        model.title = gallery['title'];
        model.image = gallery['image'];
        _galleryList.add(model);
      });
    });
  }

  _getDirectory() async {
    var directory = await getApplicationDocumentsDirectory();
    var path = directory.path;
    _imagePath = await path;
  }

  _getDialog(BuildContext context, index) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (param) {
        return CarouselSlider(
          options: CarouselOptions(
            autoPlay: false,
            enlargeCenterPage: true,
            viewportFraction: 1,
            aspectRatio: 1.0,
            initialPage: index,
            height: MediaQuery.of(context).size.height,
          ),
          items: _galleryList
              .map((item) => Container(
                    decoration: BoxDecoration(color: Colors.white),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.topCenter,
                          child: RaisedButton(
                              color: Colors.red,
                              elevation: 5.0,
                              child: Text(
                                "Kapat",
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () => Navigator.pop(context)),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  padding: EdgeInsets.all(10.0),
                                  decoration: BoxDecoration(boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(0, 3),
                                    ),
                                  ]),
                                  child: Image.file(
                                    File(_imagePath + "/" + item.image),
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  "${item.title}",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ))
              .toList(),
        );
      },
    );
  }

  _deleteFormDialog(BuildContext context, galleryId, image) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (param) {
          return AlertDialog(
            actions: [
              FlatButton(
                  color: Colors.blue,
                  onPressed: () => Navigator.pop(context),
                  child: Text("İptal")),
              FlatButton(
                  color: Colors.red,
                  onPressed: () async {
                    var result =
                        await _galleryController.deleteGallery(galleryId);
                    if (result > 0) {
                      await _galleryController.deleteImage(image);
                      Navigator.pop(context);
                      _getGallery();
                      _showSuccessSnackBar(Text("Silme işlemi Başarılı"));
                    }
                  },
                  child: Text("Sil")),
            ],
            title: Text("Silmek istediğinize emin misiniz?"),
          );
        });
  }

  _screenBody() {
    if (_galleryList.length == 0) {
      return Center(
        child: Text("Galeride Hiç Resim Yok"),
      );
    } else {
      return ListView.builder(
          itemCount: _galleryList.length,
          itemBuilder: (context, index) {
            return Container(
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3),
                  ),
                ]),
                padding: EdgeInsets.all(15.0),
                margin: EdgeInsets.fromLTRB(0, 0, 0, 8),
                child: Column(
                  children: [
                    InkWell(
                        onTap: () {
                          _getDialog(context, index);
                        },
                        child: Column(
                          children: [
                            Image.file(
                              File(
                                  _imagePath + "/" + _galleryList[index].image),
                            ),
                            SizedBox(height: 30.0),
                            Text(_galleryList[index].title,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue)),
                            SizedBox(
                              height: 20.0,
                            )
                          ],
                        )),
                    RaisedButton(
                      child: Text(
                        "Sil",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.red,
                      onPressed: () {
                        _deleteFormDialog(context, _galleryList[index].id,
                            _galleryList[index].image);
                      },
                    )
                  ],
                ));
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      appBar: AppBar(
        title: Row(
          children: [
            Icon(Icons.image),
            SizedBox(width: 5.0),
            Text(
              "Galeri",
              style: TextStyle(),
            ),
          ],
        ),
      ),
      drawer: DrawerScreen(),
      body: _screenBody(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          Navigator.pushNamed(context, "/gallery/new").then((value) async {
            if (value == "success") {
              _showSuccessSnackBar(Text("İşlem Başarılı"));
            }
            setState(() {
              print(_getGallery());
            });
          })
        },
        child: Icon(Icons.add_a_photo),
      ),
    );
  }
}
