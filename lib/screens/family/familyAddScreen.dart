import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:remindbox/controller/familyController.dart';
import 'package:remindbox/models/family.dart';
import 'package:image_picker/image_picker.dart';

class FamilyAddScreen extends StatefulWidget {
  @override
  _FamilyAddScreen createState() => _FamilyAddScreen();
}

class _FamilyAddScreen extends State<FamilyAddScreen> {
  @override
  void initState() {}

  var _titleController = TextEditingController();
  var _detailController = TextEditingController();
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  File imageFile;

  _showErrorSnackBar(message) {
    var _snackBar = SnackBar(content: message, backgroundColor: Colors.red);
    _globalKey.currentState.showSnackBar(_snackBar);
  }

  _openGallery(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      imageFile = picture as File;
    });
    Navigator.of(context).pop();
  }

  _openCamera(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      imageFile = picture as File;
    });
    Navigator.of(context).pop();
  }

  Future<void> _showChoiseDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Birini Seçin"),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  GestureDetector(
                    child: Text("Galeri"),
                    onTap: () {
                      _openGallery(context);
                    },
                  ),
                  SizedBox(height: 20.0),
                  GestureDetector(
                    child: Text("Kamera"),
                    onTap: () {
                      _openCamera(context);
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget _decideImageView() {
    if (imageFile == null) {
      return Text("Resim seçilmedi");
    } else {
      return Image.file(imageFile);
    }
  }

  _getTitleInput() {
    return TextField(
      controller: _titleController,
      decoration: InputDecoration(labelText: "Başlık", hintText: "Başlık"),
    );
  }

  _getDetailInput() {
    return TextField(
      minLines: 7,
      maxLines: 15,
      controller: _detailController,
      decoration: InputDecoration(labelText: "Detay", hintText: "Detay"),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      appBar: AppBar(
        title: Text("Aile Bireyi Ekle"),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: [
                  _getTitleInput(),
                  SizedBox(height: 20.0),
                  _decideImageView(),
                  SizedBox(height: 30.0),
                  RaisedButton(
                    child: Text("Resim Seçin"),
                    color: Colors.pinkAccent,
                    onPressed: () {
                      _showChoiseDialog(context);
                    },
                  ),
                  SizedBox(height: 20.0),
                  SingleChildScrollView(child: _getDetailInput()),
                  SizedBox(
                    height: 20.0,
                  ),
                  RaisedButton(
                    onPressed: () async {
                      var directory = await getApplicationDocumentsDirectory();
                      var path = directory.path;
                      var fileExtension = imageFile.toString().split(".").last;
                      var time = DateTime.now().millisecondsSinceEpoch;
                      var fileName = time.toString() + "." + fileExtension;
                      var copyPath = path + "/" + fileName;
                      final File newImage = await imageFile.copy('$copyPath');
                      var familyObject = Family();
                      familyObject.title = _titleController.text;
                      familyObject.detail = _detailController.text;
                      familyObject.image = fileName;
                      var _familyController = FamilyController();
                      var result =
                          await _familyController.saveFamily(familyObject);
                      if (result > 0) {
                        Navigator.pop(context, "success");
                      } else {
                        _showErrorSnackBar(
                            Text("Hata Oluştu Bilgilerinizi Kontrol Ediniz"));
                      }
                    },
                    color: Colors.blue,
                    child: Text(
                      "Kaydet",
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
