import 'package:flutter/material.dart';
import 'package:remindbox/controller/familyController.dart';
import 'package:remindbox/models/family.dart';
import 'package:remindbox/screens/drawerScreen.dart';

class FamilyListScreen extends StatefulWidget {
  @override
  _FamilyListScreenState createState() => _FamilyListScreenState();
}

class _FamilyListScreenState extends State<FamilyListScreen> {
  List<Family> _familyList = List<Family>();
  var _familyController = FamilyController();
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _getFamily();
  }

  _getFamily() async {
    _familyList.clear();
    var familys = await _familyController.getFamily();
    setState(() {
      familys.forEach((family) {
        var model = Family();
        model.id = family["id"];
        model.title = family['title'];
        model.detail = family['detail'];
        model.image = family['image'];
        _familyList.add(model);
      });
    });
  }

  _showSuccessSnackBar(message) {
    var _snackBar = SnackBar(
      content: message,
      backgroundColor: Colors.green,
    );
    _globalKey.currentState.showSnackBar(_snackBar);
  }

  _showErrorSnackBar(message) {
    var _snackBar = SnackBar(content: message, backgroundColor: Colors.red);
    _globalKey.currentState.showSnackBar(_snackBar);
  }

  _deleteFormDialog(BuildContext context, familyId) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (param) {
          return AlertDialog(
            actions: [
              FlatButton(
                  color: Colors.blue,
                  onPressed: () => Navigator.pop(context),
                  child: Text("İptal")),
              FlatButton(
                  color: Colors.red,
                  onPressed: () async {
                    var result = await _familyController.deleteFamily(familyId);
                    if (result > 0) {
                      Navigator.pop(context);
                      _getFamily();
                      _showSuccessSnackBar(Text("Silme işlemi Başarılı"));
                    } else {
                      _showErrorSnackBar(
                          Text("Hata Oluştu Bilgilerinizi Kontrol Ediniz"));
                    }
                  },
                  child: Text("Sil")),
            ],
            title: Text("Silmek istediğinize emin misiniz?"),
          );
        });
  }

  _screenBody() {
    if (_familyList.length == 0) {
      return Center(
        child: Text("Hiç Kayıt Yok"),
      );
    } else {
      return Column(
        children: [
          Expanded(
            child: ListView.builder(
                itemCount: _familyList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding:
                        const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0)),
                      elevation: 10.0,
                      child: ListTile(
                        leading: IconButton(
                          icon: Icon(
                            Icons.edit,
                            color: Colors.blue,
                          ),
                          tooltip: "Düzenle",
                          onPressed: () {
                            Navigator.pushNamed(context, "/family/edit",
                                    arguments: _familyList[index])
                                .then((value) async {
                              if (value == "success") {
                                _showSuccessSnackBar(Text("İşlem Başarılı"));
                              }
                              setState(() {
                                _getFamily();
                              });
                            });
                          },
                        ),
                        title: Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Expanded(
                                  child: InkWell(
                                      onTap: () {
                                        Navigator.pushNamed(
                                            context, "/family/detail",
                                            arguments: _familyList[index]);
                                      },
                                      child: Text(_familyList[index].title)),
                                ),
                              ),
                              IconButton(
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                  tooltip: "Sil",
                                  onPressed: () {
                                    _deleteFormDialog(
                                        context, _familyList[index].id);
                                  })
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }),
          )
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      appBar: AppBar(
        title: Row(
          children: [
            Icon(Icons.family_restroom),
            SizedBox(width: 5.0),
            Text(
              "Ailem",
              style: TextStyle(),
            ),
          ],
        ),
      ),
      drawer: DrawerScreen(),
      body: _screenBody(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          Navigator.pushNamed(context, "/family/new").then((value) async {
            if (value == "success") {
              _showSuccessSnackBar(Text("İşlem Başarılı"));
            }
            setState(() {
              _getFamily();
            });
          })
        },
        child: Icon(Icons.family_restroom),
      ),
    );
  }
}
