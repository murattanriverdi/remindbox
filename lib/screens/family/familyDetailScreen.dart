import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:remindbox/models/family.dart';
import 'package:sqflite/utils/utils.dart';

class FamilyDetailScreen extends StatefulWidget {
  @override
  _FamilyDetailScreenState createState() => _FamilyDetailScreenState();
}

class _FamilyDetailScreenState extends State<FamilyDetailScreen> {
  var _imagePath;
  _getDirectory() async {
    var directory = await getApplicationDocumentsDirectory();
    var path = directory.path;
    _imagePath = await path;
  }

  @override
  void initState() {
    _getDirectory();
  }

  final Future<String> _calculation = Future<String>.delayed(
    const Duration(seconds: 2),
    () => '',
  );
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments as Family;
    _getDirectory();
    return Scaffold(
        appBar: AppBar(
          title: Text(args.title),
        ),
        body: FutureBuilder<String>(
          future: _calculation,
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            if (snapshot.hasData) {
              return Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3),
                    ),
                  ]),
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        args.title,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 30.0),
                      Center(
                        child: Container(
                          width: MediaQuery.of(context).size.width - 100,
                          padding: EdgeInsets.all(10.0),
                          decoration: BoxDecoration(boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3),
                            ),
                          ]),
                          child: Image.file(
                            File(_imagePath + "/" + args.image),
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Text(
                        args.detail,
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ));
            } else {
              return Center(
                child: SizedBox(
                  child: CircularProgressIndicator(),
                  width: 60,
                  height: 60,
                ),
              );
            }
          },
        ));
  }
}
