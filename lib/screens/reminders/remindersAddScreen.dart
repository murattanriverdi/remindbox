import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:remindbox/controller/cronJobController.dart';
import 'package:remindbox/controller/remindersController.dart';
import 'package:remindbox/models/reminders.dart';
import 'package:remindbox/notification/NotificationManager.dart';
import 'package:remindbox/screens/NotificationPayload.dart';

class RemindersAddScreen extends StatefulWidget {
  @override
  _RemindersAddScreenState createState() => _RemindersAddScreenState();
}

class _RemindersAddScreenState extends State<RemindersAddScreen> {
  TimeOfDay time;
  var _titleController = TextEditingController();
  var _descriptionController = TextEditingController();
  var _dateController = TextEditingController(
      text: DateFormat("dd-MM-yyyy").format(DateTime.now()).toString());
  var _timeController = TextEditingController(
      text: DateFormat("HH:mm").format(DateTime.now()).toString());
  var _selectedValue = 1;
  DateTime globalDateTime;
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  DateTime _dateTime = DateTime.now();

  @override
  void initState() {
    time = TimeOfDay.now();
  }

  _selectedTodoDate() async {
    var _pickDate = await showDatePicker(
        context: context,
        locale: const Locale("tr", "TR"),
        initialDate: _dateTime,
        firstDate: DateTime.now(),
        lastDate: DateTime(2100));

    if (_pickDate != null) {
      setState(() {
        _dateTime = _pickDate;
        _dateController.text = DateFormat('dd-MM-yyyy').format(_pickDate);
      });
    }
  }

  _pickTime() async {
    TimeOfDay t = await showTimePicker(
        context: context,
        initialTime: time,
        builder: (context, child) {
          if (MediaQuery.of(context).alwaysUse24HourFormat) {
            return Localizations.override(
              context: context,
              locale: Locale('tr', 'TR'),
              child: child,
            );
          }
        });
    if (t != null)
      setState(() {
        time = t;
        print(time);
        _timeController.text = time.hour.toString().padLeft(2, '0') +
            ":" +
            time.minute.toString().padLeft(2, '0');
      });
  }

  onNotificationReceive(ReceiveNotification notification) {
    print("Notification Received: ${notification.id}");
  }

  onNotificationClick(String payload) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) {
      return NotificationPayload(
        payload: payload,
      );
    }));
  }

  _showErrorSnackBar(message) {
    var _snackBar = SnackBar(content: message, backgroundColor: Colors.red);
    _globalKey.currentState.showSnackBar(_snackBar);
  }

  _getTitleInput() {
    return TextField(
      controller: _titleController,
      decoration: InputDecoration(labelText: "Başlık", hintText: "Başlık"),
    );
  }

  _getDescriptionInput() {
    return TextField(
      controller: _descriptionController,
      decoration: InputDecoration(labelText: "Açıklama", hintText: "Açıklama"),
    );
  }

  _getDateTimeInput() {
    return TextField(
      onTap: () {
        _selectedTodoDate();
      },
      controller: _dateController,
      decoration: InputDecoration(
          labelText: "Tarih",
          hintText: "Tarih",
          prefixIcon: InkWell(
            child: Icon(Icons.calendar_today),
          )),
    );
  }

  _getTimeInput() {
    return TextField(
      onTap: () {
        _pickTime();
      },
      controller: _timeController,
      decoration: InputDecoration(
          labelText: "Saat",
          hintText: "Saat",
          prefixIcon: InkWell(
            child: Icon(Icons.watch),
          )),
    );
  }

  _getRepeatTypeSelect() {
    return DropdownButtonFormField(
      value: _selectedValue,
      items: <DropdownMenuItem>[
        DropdownMenuItem(
          child: Text("Tekrar Yok"),
          value: 0,
        ),
        DropdownMenuItem(
          child: Text("Günlük"),
          value: 1,
        ),
        DropdownMenuItem(
          child: Text("Haftalık"),
          value: 2,
        ),
      ],
      hint: Text('Tekrar Tipi'),
      onChanged: (value) {
        setState(() {
          _selectedValue = value;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      appBar: AppBar(
        title: Text("Hatırlatma Ekle"),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: [
                  _getTitleInput(),
                  _getDescriptionInput(),
                  _getDateTimeInput(),
                  _getTimeInput(),
                  SizedBox(
                    height: 14.0,
                  ),
                  Text(
                    "Tekrar Tipi",
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 4.0,
                  ),
                  _getRepeatTypeSelect(),
                  SizedBox(
                    height: 20.0,
                  ),
                  RaisedButton(
                    onPressed: () async {
                      var reminderObject = Reminders();
                      reminderObject.title = _titleController.text;
                      reminderObject.description = _descriptionController.text;
                      reminderObject.date = _dateController.text;
                      reminderObject.time = _timeController.text;
                      reminderObject.repeatType = _selectedValue;
                      var _reminderController = RemindersController();
                      var _cronJobController = CronJobController();
                      var result = await _reminderController
                          .saveReminder(reminderObject);
                      if (result['response'] > 0) {
                        notificationManager
                            .setOnNotificationClick(onNotificationClick);
                        notificationManager
                            .setOnNotificationReceive(onNotificationReceive);
                        await notificationManager
                            .createNotification(reminderObject);
                        Navigator.pop(context);
                      } else {
                        _showErrorSnackBar(
                            Text("Hata Oluştu Bilgilerinizi Kontrol Ediniz"));
                      }
                    },
                    color: Colors.blue,
                    child: Text(
                      "Kaydet",
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
