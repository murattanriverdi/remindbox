import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Drawer(
        child: ListView(
          children: [
            ListTile(
              leading: Icon(Icons.home),
              title: Text("Anasayfa"),
              onTap: () {
                Navigator.pushNamed(context, "/");
              },
            ),
            ListTile(
              leading: Icon(Icons.family_restroom),
              title: Text('Ailem'),
              onTap: () {
                Navigator.pushNamed(context, "/family");
              },
            ),
            ListTile(
              leading: Icon(Icons.photo_sharp),
              title: Text('Galeri'),
              onTap: () {
                Navigator.pushNamed(context, "/gallery");
              },
            ),
            ListTile(
              leading: Icon(Icons.record_voice_over),
              title: Text('Ses Kayıtları'),
              onTap: () {
                Navigator.pushNamed(context, "/audio/new");
              },
            ),
          ],
        ),
      ),
    );
  }
}
