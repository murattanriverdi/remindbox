import 'package:flutter/material.dart';

class NotificationPayload extends StatefulWidget {
  final String payload;

  NotificationPayload({this.payload});

  @override
  _NotificationPayloadState createState() => _NotificationPayloadState();
}

class _NotificationPayloadState extends State<NotificationPayload> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(widget.payload),
      ),
    );
  }
}
