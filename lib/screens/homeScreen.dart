import 'package:cron/cron.dart';
import 'package:flutter/material.dart';
import 'package:remindbox/controller/remindersController.dart';
import 'package:remindbox/models/reminders.dart';
import 'package:remindbox/screens/drawerScreen.dart';
import 'package:remindbox/worker/cronJobOperation.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Reminders> _reminderList = List<Reminders>();
  var _remindersController = RemindersController();
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    var cron = new Cron();
    cron.schedule(new Schedule.parse('0 0 * * *'), () async {
      var _cronjonOperation = CronJobOperation(context);
      _cronjonOperation.cronJobControl();
    });

    super.initState();
    _getReminders();
  }

  _getReminders() async {
    _reminderList.clear();
    var reminders = await _remindersController.getReminders();
    setState(() {
      reminders.forEach((reminder) {
        var model = Reminders();
        model.id = reminder["id"];
        model.title = reminder['title'];
        model.description = reminder['description'];
        model.date = reminder['date'];
        model.time = reminder['time'];
        model.repeatType = reminder['repeatType'];
        _reminderList.add(model);
      });
    });
  }

  _showSuccessSnackBar(message) {
    var _snackBar = SnackBar(
      content: message,
      backgroundColor: Colors.green,
    );
    _globalKey.currentState.showSnackBar(_snackBar);
  }

  _showErrorSnackBar(message) {
    var _snackBar = SnackBar(content: message, backgroundColor: Colors.red);
    _globalKey.currentState.showSnackBar(_snackBar);
  }

  _deleteFormDialog(BuildContext context, reminderId) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (param) {
          return AlertDialog(
            actions: [
              FlatButton(
                  color: Colors.blue,
                  onPressed: () => Navigator.pop(context),
                  child: Text("İptal")),
              FlatButton(
                  color: Colors.red,
                  onPressed: () async {
                    var result =
                        await _remindersController.deleteReminders(reminderId);
                    if (result > 0) {
                      Navigator.pop(context);
                      _getReminders();
                      _showSuccessSnackBar(Text("Silme işlemi Başarılı"));
                    } else {
                      _showErrorSnackBar(
                          Text("Hata Oluştu Bilgilerinizi Kontrol Ediniz"));
                    }
                  },
                  child: Text("Sil")),
            ],
            title: Text("Silmek istediğinize emin misiniz?"),
          );
        });
  }

  _bodyScreen() {
    if (_reminderList.length == 0) {
      return Center(
        child: Text("Hatırlatma yok"),
      );
    } else {
      return Column(
        children: [
          Expanded(
            child: ListView.builder(
                itemCount: _reminderList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding:
                        const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0)),
                      elevation: 10.0,
                      child: ListTile(
                        title: Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Expanded(
                                  child: Text(_reminderList[index].title),
                                ),
                              ),
                              IconButton(
                                  icon: Icon(Icons.delete),
                                  color: Colors.red,
                                  tooltip: "Sil",
                                  onPressed: () {
                                    _deleteFormDialog(
                                        context, _reminderList[index].id);
                                  })
                            ],
                          ),
                        ),
                        subtitle: Column(
                          children: [
                            SizedBox(height: 5.0),
                            Row(children: [
                              Container(
                                  child: Expanded(
                                      child: Text(
                                          _reminderList[index].description))),
                            ]),
                            SizedBox(height: 15.0),
                            Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.only(bottom: 10.0),
                                  child: Text(_reminderList[index].date +
                                      " " +
                                      _reminderList[index].time),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          )
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      appBar: AppBar(
        title: Row(
          children: [
            Icon(Icons.notifications),
            SizedBox(width: 5.0),
            Text(
              "Hatırlatmalar",
              style: TextStyle(),
            ),
          ],
        ),
      ),
      drawer: DrawerScreen(),
      body: _bodyScreen(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          Navigator.pushNamed(context, "/reminders/new").then((value) async {
            if (value == "success") {
              _showSuccessSnackBar(Text("İşlem Başarılı"));
            }
            setState(() {
              _getReminders();
            });
          })
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
