import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:remindbox/router/routes.dart';
import 'package:remindbox/screens/homeScreen.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [GlobalMaterialLocalizations.delegate],
      supportedLocales: [const Locale('fr')],
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
      routes: Routes.routes,
    );
  }
}
