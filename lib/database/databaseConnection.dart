import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseConnection {
  setDatabase() async {
    var directory = await getApplicationDocumentsDirectory();
    var path = join(directory.path, 'db_reminders_app');
    var database =
        await openDatabase(path, version: 1, onCreate: _onCreatingDatabase);
    return database;
  }

  _onCreatingDatabase(Database database, int version) async {
    await database.execute(
        "CREATE TABLE reminders(id INTEGER PRIMARY KEY,title TEXT,description TEXT,date TEXT,time TEXT,repeatType int,repeatReminderMinute int)");
    await database.execute(
        "CREATE TABLE cron_job(id INTEGER PRIMARY KEY,reminder_id INTEGER, FOREIGN KEY (reminder_id) REFERENCES reminders (id) ON DELETE CASCADE)");
    await database.execute(
        "CREATE TABLE gallery(id INTEGER PRIMARY KEY,title TEXT,image TEXT)");
    await database.execute(
        "CREATE TABLE family(id INTEGER PRIMARY KEY,title TEXT,detail TEXT,image TEXT)");
  }
}
