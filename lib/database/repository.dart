import 'package:remindbox/database/databaseConnection.dart';
import 'package:sqflite/sqflite.dart';

class Repository {
  DatabaseConnection _databaseConnection;
  static Database _database;
  Repository() {
    _databaseConnection = DatabaseConnection();
  }

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _databaseConnection.setDatabase();
    return _database;
  }

  getData(table) async {
    var connection = await database;
    return await connection.query(table);
  }

  getDataById(table, id) async {
    var connection = await database;
    return await connection.query(table, where: 'id', whereArgs: [id]);
  }

  insertData(table, data) async {
    var connection = await database;
    return await connection.insert(table, data);
  }

  deleteData(table, itemId) async {
    var connection = await database;
    return await connection.rawDelete("DELETE FROM $table WHERE id =$itemId");
  }

  updateData(table, data) async {
    var connection = await database;
    return await connection
        .update(table, data, where: 'id=?', whereArgs: [data['id']]);
  }

//sadece cronjob için
  getCronJobs() async {
    var connection = await database;
    return await connection.rawQuery(
        "SELECT cron_job.id,reminders.id as reminder_id,reminders.date,reminders.time,reminders.title,reminders.description,reminders.repeatType FROM cron_job INNER JOIN reminders ON reminders.id=cron_job.reminder_id");
  }
}
