import 'package:flutter/cupertino.dart';
import 'package:remindbox/screens/family/familyAddScreen.dart';
import 'package:remindbox/screens/family/familyDetailScreen.dart';
import 'package:remindbox/screens/family/familyEditScreen.dart';
import 'package:remindbox/screens/family/familyListScreen.dart';
import 'package:remindbox/screens/gallery/galleryAddScreen.dart';
import 'package:remindbox/screens/gallery/galleryListScreen.dart';
import 'package:remindbox/screens/reminders/remindersAddScreen.dart';

class Routes {
  static final routes = <String, WidgetBuilder>{
    //reminders page
    '/reminders/new': (BuildContext context) => RemindersAddScreen(),
    //gallery page
    '/gallery': (BuildContext context) => GalleryListScreen(),
    '/gallery/new': (BuildContext context) => GalleryAddScreen(),
    //family page
    '/family': (BuildContext context) => FamilyListScreen(),
    '/family/new': (BuildContext context) => FamilyAddScreen(),
    '/family/detail': (BuildContext context) => FamilyDetailScreen(),
    '/family/edit': (BuildContext context) => FamilyEditScreen(),
  };
}
