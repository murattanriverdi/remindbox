class Reminders {
  int id;
  String title;
  String description;
  String date;
  String time;
  int repeatType;

  remindersMap() {
    var mapping = Map<String, dynamic>();
    mapping['id'] = id;
    mapping['title'] = title;
    mapping['description'] = description;
    mapping['date'] = date;
    mapping['time'] = time;
    mapping['repeatType'] = repeatType; // 0=>yok,1=>günlük,2=> haftalık
    return mapping;
  }
}
