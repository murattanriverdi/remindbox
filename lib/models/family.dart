class Family {
  int id;
  String title;
  String detail;
  String image;

  FamilyMap() {
    var mapping = Map<String, dynamic>();
    mapping['id'] = id;
    mapping['title'] = title;
    mapping['detail'] = detail;
    mapping['image'] = image;

    return mapping;
  }
}
