import 'package:intl/intl.dart';

class DateOperation {
  dateConvertOperation(String date) {
    DateTime tempDate = new DateFormat("dd-MM-yyyy HH:mm").parse(date);
    DateTime newDate =
        new DateFormat('yyyy-MM-dd HH:mm').parse(tempDate.toString());
    // newDate = newDate.add(Duration(microseconds: 001));
    return newDate;
  }
}
