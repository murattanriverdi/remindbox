import 'package:remindbox/controller/cronJobController.dart';
import 'package:remindbox/models/cronJob.dart';
import 'package:remindbox/models/reminders.dart';
import 'package:remindbox/notification/NotificationManager.dart';
import 'package:flutter/material.dart';
import 'package:remindbox/screens/NotificationPayload.dart';
import 'package:remindbox/worker/dateOperation.dart';

class CronJobOperation {
  BuildContext context;
  List<CronJob> _cronjobList = List<CronJob>();
  CronJobController _cronJobController = CronJobController();

  CronJobOperation(BuildContext context) {
    this.context = context;
    notificationManager.setOnNotificationReceive(onNotificationReceive);
    notificationManager.setOnNotificationClick(onNotificationClick);
  }
  cronJobControl() async {
    var cronJobs = await _getCronJobs();
    var _cronJobController = CronJobController();
    cronJobs.forEach((cronJob) async {
      DateTime date = DateOperation()
          .dateConvertOperation(cronJob['date'] + " " + cronJob['time']);
      DateTime now = DateTime.now();
      if (date.day == now.day) {
        Reminders reminder = Reminders();
        reminder.id = cronJob['reminder_id'];
        reminder.title = cronJob['title'];
        reminder.description = cronJob['description'];
        reminder.date = cronJob['date'];
        reminder.time = cronJob['time'];
        reminder.repeatType = cronJob['repeatType'];
        await notificationManager.createNotification(reminder);
        _cronJobController.deleteCronJob(cronJob['id']);
      }
    });
  }

  _getCronJobs() async {
    var cronJobs = await _cronJobController.getCronJobs();
    return cronJobs;
  }

  onNotificationReceive(ReceiveNotification notification) {
    print("Notification Received: ${notification.id}");
  }

  onNotificationClick(String payload) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) {
      return NotificationPayload(
        payload: payload,
      );
    }));
  }
}
