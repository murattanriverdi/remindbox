import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:remindbox/database/repository.dart';
import 'package:remindbox/models/gallery.dart';

class GalleryController {
  final String tableName = "gallery";
  Repository _repository;
  GalleryController() {
    _repository = Repository();
  }

  Future<List> getGallery() async {
    return await _repository.getData(tableName);
  }

  saveGallery(Gallery gallery) async {
    var result = await _repository.insertData(tableName, gallery.galleryMap());
    return result;
  }

  deleteGallery(galleryId) async {
    var status = await _repository.deleteData(tableName, galleryId);
    return status;
  }

  Future<File> _filePath(image) async {
    final directory = await getApplicationDocumentsDirectory();
    final path = await directory.path;
    String fileName = path + "/" + image;
    return File(fileName);
  }

  deleteImage(image) async {
    final file = await _filePath(image);
    file.delete();
  }
}
