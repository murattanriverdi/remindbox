import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:remindbox/database/repository.dart';
import 'package:remindbox/models/family.dart';

class FamilyController {
  final String tableName = "family";
  Repository _repository;
  FamilyController() {
    _repository = Repository();
  }

  Future<List> getFamily() async {
    return await _repository.getData(tableName);
  }

  saveFamily(Family family) async {
    var result = await _repository.insertData(tableName, family.FamilyMap());
    return result;
  }

  deleteFamily(familyId) async {
    var status = await _repository.deleteData(tableName, familyId);
    return status;
  }

  updateFamily(Family family) async {
    return await _repository.updateData(tableName, family.FamilyMap());
  }
}
