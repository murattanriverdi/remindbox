import 'package:flutter/cupertino.dart';
import 'package:remindbox/controller/cronJobController.dart';
import 'package:remindbox/database/repository.dart';
import 'package:remindbox/models/cronJob.dart';
import 'package:remindbox/models/reminders.dart';
import 'package:remindbox/notification/NotificationManager.dart';
import 'package:remindbox/worker/cronJobOperation.dart';
import 'package:remindbox/worker/dateOperation.dart';

class RemindersController {
  final String tableName = "reminders";
  Repository _repository;
  BuildContext context;
  RemindersController() {
    _repository = Repository();
  }

  Future<List> getReminders() async {
    return await _repository.getData(tableName);
  }

  saveReminder(Reminders reminders) async {
    var result = {'response': "", 'notification': false};
    var response =
        await _repository.insertData(tableName, reminders.remindersMap());
    result['response'] = response;
    if (response > 0) {
      reminders.id = response;
      DateTime date = DateOperation()
          .dateConvertOperation(reminders.date + " " + reminders.time);
      DateTime now = DateTime.now();
      //tarih bugun mu
      if (date.day == now.day) {
        result['notification'] = true;
      } else {
        DateTime addedDay =
            DateTime.now().add(Duration(days: 1)); //tarihe 1 gun ekledik;
        if (addedDay.compareTo(date) == 1) {
          result['notification'] = true;
        } else {
          var cronjob = CronJob();
          cronjob.reminder_id = reminders.id;
          var _cronJobController = CronJobController();
          await _cronJobController.saveCronJob(cronjob);
        }
      }
    }
    return result;
  }

  deleteReminders(reminderId) async {
    var result = await _repository.deleteData(tableName, reminderId);
    if (result > 0) {
      notificationManager.flutterLocalNotificationsPlugin.cancel(reminderId);
    }
    return result;
  }
}
