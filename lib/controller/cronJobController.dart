import 'package:remindbox/database/repository.dart';
import 'package:remindbox/models/cronJob.dart';

class CronJobController {
  final String tableName = "cron_job";
  Repository _repository;
  CronJobController() {
    _repository = Repository();
  }

  Future<List> getCronJobs() async {
    return await _repository.getCronJobs();
  }

  saveCronJob(CronJob cronjob) async {
    var result = await _repository.insertData(tableName, cronjob.cronJobMap());
    if (result > 0) {}
    return result;
  }

  deleteCronJob(cronJobId) async {
    return await _repository.deleteData(tableName, cronJobId);
  }
}
